#!/bin/bash -x
set -e  # error on failure

# Use first parameter for image version or default to eap
version=${1:-eap}

# Simple client version
docker run -ti bobswiftapps/acli:${version} acli -a getClientInfo --outputFormat 2

# Test mounting acli.properties
# Mount into installation directory
docker run -v ${PWD}/acli.properties:/opt/atlassian-cli/acli.properties -ti bobswiftapps/acli:${version} acli -a getServerInfo

# Mount anywhere and set location with environment variable ACLI_CONFIG
docker run -v ${PWD}/acli.properties:/tmp/acli.properties -e ACLI_CONFIG=/tmp/acli.properties -ti bobswiftapps/acli:${version} acli -a getServerInfo --outputFormat 2

# Test setting environment variables with a file - run manually
#docker run --env-file=.env -ti bobswiftapps/acli:eap /bin/bash
# acli ${examplegear}            -a getServerInfo
# acli ${examplegear_confluence} -a getServerInfo
