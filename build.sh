#!/bin/bash -x
set -e  # error on failure

# Use first parameter for image version or default to eap
version=${1:-eap}

# Build and test
docker build --tag bobswiftapps/acli:${version} .
docker run -ti bobswiftapps/acli:${version} acli -a getClientInfo --outputFormat 2
