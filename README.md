![ACLI](https://appfire.atlassian.net/wiki/download/attachments/86083606/Appfire_CLI.svg)

# Overview
The Appfire CLI (ACLI), formerly known as the Atlassian CLI, is an integrated family of CLI's for various Atlassian and other applications providing a consistent and reliable automation platform for advanced users, administrators, script writers, and DevOps developers. This provides a convenient way to quickly do tasks, implement business processes, or general automation with your various popular applications. The CLIs are built on remote APIs provided by the applications and deliver a higher level, client based API that is easier to use than the underlying product APIs. See [ACLI](https://apps.appf.re/acli) for more details.

We use the term client to refer to the support specific to an application like the Jira client, Confluence client, Bitbucket client, or the Slack client for example. All clients are included in the ACLI distributions and Docker images.

The ACLI Docker image provides a convenient way to quickly install the client distribution and start using the ACLI immediately! Starting with 10.3.0, there are multiple flavors of the ACLI Docker image depending on your use case. All images contain the ACLI and work in the same way. However, there are differences in what is included in the images to make them as convenient as possible for specific DevOps and pipeline use case. Appfire uses these images internally for integration testing pipelines and related automations. Also starting with 10.3.0 and higher, images are now based on current LTS versions of **Ubuntu** and include a few additional tools especially helpful for pipelines and DevOps scenarios. Currently, those are: **git nano unzip**.

# Images
- [acli](https://hub.docker.com/r/bobswiftapps/acli) - ACLI optimized distribution (Linux/amd64). Note this no longer includes Java JRE as of 10.3.0 or higher.
- [acli-jre](https://hub.docker.com/r/bobswiftapps/acli-jre) - ACLI optimized distribution (Linux/amd64) including a Java 17 JRE from [Eclipse Temurin](https://hub.docker.com/_/eclipse-temurin).
- [acli-generic](https://hub.docker.com/r/bobswiftapps/acli-jre) - ACLI generic distribution including a Java 17 JRE from [Eclipse Temurin](https://hub.docker.com/_/eclipse-temurin).

# Links
- [ACLI](https://apps.appf.re/acli) - general documentation
- [ACLI Distributions](https://appfire.atlassian.net/wiki/spaces/ACLI/pages/60555775/ACLI+Distributions) - explains ACLI distributions and differences
- [ACLI Action Reference](https://apps.appf.re/acli-docs) - references and examples for each client
- [ACLI Release Notes](https://apps.appf.re/acli-release-notes)
- [ACLI Support Portal](https://apps.appf.re/acli-support)
- [ACLI Docker Repository](https://hub.docker.com/r/bobswiftapps/acli/)
- [ACLI on Atlassian Community](https://apps.appf.re/acli-community) - questions and discussion

# Quick Start
To quickly get started running an ACLI Docker instance, use the following command:
```
$ docker run -ti bobswiftapps/acli:latest /bin/bash

bash-4.4# acli -a getClientInfo
Client name: cli, Client version: 10.6.0, Client build: 2023-01-25T11:20:33, Java: Substrate VM 17.0.4, OS: Linux 5.10.104-linuxkit
```

Alternately, you can run a CLI action directly:
```
$ docker run -ti bobswiftapps/acli:latest acli -a getClientInfo
Client name: cli, Client version: 10.6.0, Client build: 2023-01-25T11:20:33, Java: Substrate VM 17.0.4, OS: Linux 5.10.104-linuxkit
```

# Pass Environment Variables
```
$ docker run -e examplegear='jira -s https://examplegear.atlassian.net -u anonymous' -ti bobswiftapps/acli:latest /bin/bash
bash-4.4# acli ${examplegear} -a getServerInfo
Jira version: 1001.0.0-SNAPSHOT, build: 100207, time: 10/7/22, 3:33 AM, description: Appfire Example Gear Jira, url: https://examplegear.atlassian.net
```

# Pass Environment Variables Using An Environment File
```
$cat .env
examplegear=jira -s https://examplegear.atlassian.net -u anonymous
examplegear_confluence=confluence -s https://examplegear.atlassian.net/wiki -u anonymous
```
```
$ docker run --env-file=.env -ti bobswiftapps/acli:latest /bin/bash
bash-5.0# acli $examplegear -a getServerInfo
Jira version: 1001.0.0-SNAPSHOT, build: 100207, time: 10/7/22, 3:33 AM, description: Appfire Example Gear Jira, url: https://examplegear.atlassian.net
bash-5.0# acli $examplegear_confluence -a getServerInfo
Confluence version: 1000.0.0, build: d4f5af0175c86895f923fbf93e073a787e005e32, url: https://examplegear.atlassian.net/wiki
```

# Provide Configuration Properties with a Volume
From current directory to image's ACLI installation directory
```
docker run -v ${PWD}/acli.properties:/opt/acli/acli.properties -ti bobswiftapps/acli:latest

or prior to version 11.0.0, use:

docker run -v ${PWD}/acli.properties:/opt/atlassian-cli/acli.properties -ti bobswiftapps/acli:latest
```

From current directory to another location and reference it with ACLI configuration environment variable
```
docker run -v ${PWD}/acli.properties:/tmp/acli.properties -e ACLI_CONFIG=/tmp/acli.properties -ti bobswiftapps/acli:latest acli -a getServerInfo --outputFormat 2
```

# Versioning
Use a specific version of the ACLI by using a version number tag: **`bobswiftapps/acli:10.6.0`**. This will install the **`10.6.0`** version. This approach is recommended for production use.

Alternatively, the **`latest`** tag matches the most recent stable release of the ACLI. So **`bobswiftapps/acli:latest`** will use the latest released version of ACLI. Note that if you have previously installed using `latest`, you may first want to update your local image cache to a newer image using **`docker pull bobswiftapps/acli:latest`**.

For the latest EAP (early access program) release, use the **`eap`** tag: **`bobswiftapps/acli:eap`**. This will install our latest SNAPSHOT release that is currently under development. Note that if you have previously installed using `eap`, you should first update your local cache to the latest SNAPSHOT image available using **`docker pull bobswiftapps/acli:eap`**.
