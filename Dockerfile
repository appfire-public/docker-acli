FROM @fromImage@

ARG ACLI=acli-@version@
ARG DISTRIBUTION=distribution.zip

ENV ACLI_HOME=/opt/acli
ENV PATH="${ACLI_HOME}:${PATH}"

# Add some convenience apps to the default image
@extraTools@

# Update and upgrade packages to apply security patches
RUN apt-get update && apt-get upgrade -y && apt-get clean && rm -rf /var/lib/apt/lists/*

ARG TARGETARCH
ADD @downloadUrl@ /opt/${DISTRIBUTION}

# Unzip distribution, remove zip, find the restored directory and move it to ACLI home
# use head -n 1 to only find first (the directory) in case there is another name match like jar name
RUN unzip /opt/${DISTRIBUTION} -d /opt \
    && rm /opt/${DISTRIBUTION} \
    && export NAME="$(find /opt -name 'acli-*' | head -n 1)" \
    && echo name: $NAME \
    && mv ${NAME} ${ACLI_HOME} \
    @linkCmd@
# TODO: consider deprecate acli.sh link in version 11?? Legacy images (<= 10.2) lined acli -> acli.sh as we shipped acli.sh

WORKDIR ${ACLI_HOME}

@startCmd@
